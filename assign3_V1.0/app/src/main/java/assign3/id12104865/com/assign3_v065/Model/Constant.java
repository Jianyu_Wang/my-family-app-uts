package assign3.id12104865.com.assign3_v065.Model;

/**
 * Created by Jianyu1 on 15/10/20.
 */
public class Constant {
    public static final String NAME = "username";
    public static final String MEMBER_NAME = "name";
    public static final String MEMBER_NAMES = "names";
    public static final String PASSWORD = "password";
    public static final String CODE = "familyCode";
    public static final String IS_CREATOR = "isCreator";
    public static final String BIRTHDAY = "birthday";
    public static final String MESSAGE = "message";
    public static final String WISH_TITLE = "wishTitle";
    public static final String EMAIL = "email";
    public static final String PARAMETER_LOGIN_TAG = "logindata";
    public static final String PARAMETER_REGISTER_TAG = "data";
    public static final String PARAMETER_REFRESH_TAG = "familydata";
    public static final String RESULT_TAG = "result";
    public static final String ERROR_TAG = "errorMsg";
}
