package assign3.id12104865.com.assign3.view.service;

import java.util.ArrayList;

/**
 * Created by Jianyu1 on 15/9/26.
 */
public interface UserService {
    public int userLogin(String username, String password) throws Exception;
    public void register(String user_name,String password,String family_code,String isCreator,String famCreator) throws Exception;
    public String refreshFamilyList(String user_name,String password) throws Exception;
}
