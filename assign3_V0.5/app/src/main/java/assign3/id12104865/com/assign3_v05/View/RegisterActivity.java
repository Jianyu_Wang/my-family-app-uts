
package assign3.id12104865.com.assign3_v05.View;

import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.TextView;
import android.widget.Toast;

import java.util.Random;

import assign3.id12104865.com.assign3_v05.R;

/**
 * RegisterActivity allowed user register for this app.
 *  User can set their name and password.
 *  User can choose join or create a family.
 *  If user wanna join a family, he/she should provide the familycode.
 */

public class RegisterActivity extends AppCompatActivity {

    private static final int LENGTH_OF_CODE = 6;
    private EditText mUser_name;
    private EditText mPassword;
    private CheckBox mCreate_checkbox;
    private CheckBox mJoin_checkbox;
    private TextView mCode_text;
    private EditText mCode_edit;
    private ImageButton mCancel_imagebutton;
    private ImageButton mRegister_imagebutton;
    //private UserService mUserService;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register);
        // init button edit text and text view.
        init();
        // set Listeners to every button and check box.
        setListeners();
    }

    /**
     * This method init the button and edittext in login activity by Id.
     */
    private void init(){
        mCreate_checkbox = (CheckBox)findViewById(R.id.checkBox_create_family_RegisterActivity);
        mCreate_checkbox.setChecked(true);
        mJoin_checkbox = (CheckBox)findViewById(R.id.checkBox_join_family_RegisterActivity);
        mCode_text = (TextView)findViewById(R.id.textView_family_code_RegisterActivity);
        mCode_edit = (EditText)findViewById(R.id.editText_family_code_RegisterActivity);
        mCancel_imagebutton = (ImageButton)findViewById(R.id.imageButton_cancel_RegisterActivity);
        mRegister_imagebutton = (ImageButton)findViewById(R.id.imageButton_register_RegisterActivity);
        mUser_name = (EditText)findViewById(R.id.editText_username_RegisterActivity);
        mPassword = (EditText)findViewById(R.id.editText_password_RegisterActivity);
    }

    /**
     *  This method set the Listeners for every button and check box.
     */
    private void setListeners(){
        // create Check box
        mCreate_checkbox.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if(mCreate_checkbox.isChecked()){
                    mJoin_checkbox.setChecked(false);
                    cleanCreAndCode();
                    goneFamilyRegisiter();
                }else{
                    mJoin_checkbox.setChecked(true);
                    visibleFamilyRegister();
                }
            }
        });
        // join Check box
        mJoin_checkbox.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if(mJoin_checkbox.isChecked()){
                    mCreate_checkbox.setChecked(false);
                    visibleFamilyRegister();
                }else{
                    mCreate_checkbox.setChecked(true);
                    cleanCreAndCode();
                    goneFamilyRegisiter();
                }
            }
        });
        // cancel button
        mCancel_imagebutton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
        // register button
//        mRegister_imagebutton.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                String user_name = mUser_name.getText().toString();
//                String user_password = mPassword.getText().toString();
//                // check register information and connect to servlet
//                if (user_name.length() > EMPTY_EDIT_TEXT && user_password.length() > EMPTY_EDIT_TEXT) {
//                    if (mCreate_checkbox.isChecked()) {
//                        String fam_code = randomCode();
//                        String fam_cre = user_name;
//                        if (checkFamCode(fam_code)) {
//                            new registerTask(user_name, user_password, fam_code, IS_CREATOR, fam_cre).execute();
//                        } else {
//                            Toast.makeText(RegisterActivity.this, R.string.code_create_error, Toast.LENGTH_LONG).show();
//                        }
//                    } else {
//                        String fam_code = mCode_edit.getText().toString();
//                        String fam_cre = mCreate_edit.getText().toString();
//                        if (checkFamCode(fam_code) && fam_cre.length() != EMPTY_EDIT_TEXT) {
//                            new registerTask(user_name, user_password, fam_code, IS_NOT_CREATOR, fam_cre).execute();
//                        } else {
//                            Toast.makeText(RegisterActivity.this, R.string.code_input_error, Toast.LENGTH_LONG).show();
//                        }
//                    }
//                } else {
//                    Toast.makeText(RegisterActivity.this, R.string.register_details_fill_error, Toast.LENGTH_LONG).show();
//                }
//            }
//        });
    }

    /**
     * clean family creator and family code text view.
     */
    private void cleanCreAndCode(){
        mCode_edit.setText("");
    }

    /**
     * create random code.
     * @return a random number.
     */
    private String randomCode(){
        Random random = new Random();
        String result = "";
        for (int i = 0; i < LENGTH_OF_CODE; i++){
            result += random.nextInt(10);
        }
        return result;
    }

    /**
     *
     * @param famcode that input from user
     * @return is the famcode regular (6 number)
     */
    private boolean checkFamCode(String famcode){
        if(famcode.length() == LENGTH_OF_CODE){
            try {
                Integer.parseInt(famcode);
                return true;
            } catch (NumberFormatException e) {
                return false;
            }
        }else{
            return false;
        }
    }

    /**
     * Show code text view and edit view then user can input family code
     */
    public void visibleFamilyRegister(){
        mCode_text.setVisibility(View.VISIBLE);
        mCode_edit.setVisibility(View.VISIBLE);
    }

    /**
     * Gone code text view and edit view then user cannot input family code
     */
    public void goneFamilyRegisiter(){
        mCode_text.setVisibility(View.GONE);
        mCode_edit.setVisibility(View.GONE);
    }

    // asyntask for register
    private class registerTask extends AsyncTask<Void,Void,Void> {

        private String mUsername;
        private String mPassword;
        private String mFamilyCode;
        private String mIsCreator;
        private String mFamCreator;
        private String str = getString(R.string.success_register);

        public registerTask(String username,String password,String code,String iscreator,String creator){
            mUsername = username;
            mPassword = password;
            mFamilyCode = code;
            mIsCreator = iscreator;
            mFamCreator = creator;
        }

        @Override
        protected Void doInBackground(Void... params) {
            try {
                mUserService.register(mUsername,mPassword,mFamilyCode,mIsCreator,mFamCreator);
            } catch (ServiceRulesException e) {
                str = e.toString();
            } catch (Exception e) {
                str = e.toString();
            }
            return null;
        }

        @Override
        protected void onProgressUpdate(Void... values) {
            super.onProgressUpdate(values);
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            finish();
            Toast.makeText(RegisterActivity.this, str, Toast.LENGTH_LONG).show();
            super.onPostExecute(aVoid);
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_register, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
}
