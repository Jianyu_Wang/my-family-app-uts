package assign3.id12104865.com.assign3_v065.Service;

/**
 * Created by Jianyu1 on 15/10/20.
 */
public class ServiceRulesException extends Exception {
    public ServiceRulesException(String msg){
        super(msg);
    }
}
