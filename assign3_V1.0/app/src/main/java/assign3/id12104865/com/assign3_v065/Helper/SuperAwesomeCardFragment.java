/*
 * Copyright (C) 2013 Andreas Stuetz <andreas.stuetz@gmail.com>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package assign3.id12104865.com.assign3_v065.Helper;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.view.ViewCompat;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import assign3.id12104865.com.assign3_v065.R;
import butterknife.ButterKnife;
import butterknife.InjectView;

/**
 * SuperAwesomeCardFragment support to every card in page view
 */
public class SuperAwesomeCardFragment extends Fragment {

	private static final String ARG_POSITION = "position";

	@InjectView(R.id.textView_cardFragment)
	TextView mTextView;

	private int mPosition;
	private String mDetails;

	public SuperAwesomeCardFragment (int position,String details) {
		mDetails = details;
		Bundle b = new Bundle();
		b.putInt(ARG_POSITION, position);
		setArguments(b);
	}

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		mPosition = getArguments().getInt(ARG_POSITION);
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_card,container,false);
		ButterKnife.inject(this, rootView);
        ViewCompat.setElevation(rootView, 50);
		mTextView.setText(mDetails);
		return rootView;
	}
}