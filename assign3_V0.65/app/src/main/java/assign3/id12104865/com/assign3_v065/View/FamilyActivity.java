package assign3.id12104865.com.assign3_v065.View;

import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.ImageButton;
import android.widget.ListView;

import java.util.ArrayList;
import com.baoyz.widget.PullRefreshLayout;
import com.getbase.floatingactionbutton.FloatingActionsMenu;

import assign3.id12104865.com.assign3_v065.Adapter.MemberAdapter;
import assign3.id12104865.com.assign3_v065.Model.Canstant;
import assign3.id12104865.com.assign3_v065.Model.MemberData;
import assign3.id12104865.com.assign3_v065.R;
import assign3.id12104865.com.assign3_v065.Service.UserService;
import assign3.id12104865.com.assign3_v065.Service.UserServiceImpl;

public class FamilyActivity extends AppCompatActivity {

    private ListView mListView;
    //private FloatingActionButton mFab;
    private ArrayList<MemberData> memberDatas = new ArrayList<MemberData>();
    private MemberAdapter mAdapter;
    private String mUserName;
    private String mUserPassword;
    private PullRefreshLayout mPullRefreshLayout;
    private UserService mUserService = new UserServiceImpl();
    private View mFabA;
    private View mFabB;
    private FloatingActionsMenu menuMultipleActions;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_family);
        // init the widgets
        init();
        // get data from last activity
        getData();
        // fill the list data
        fillData();
        // create new adapter
        mAdapter = new MemberAdapter(this,memberDatas);
        // set adapter for list
        mListView.setAdapter(mAdapter);
        // set the PullRefreshList
        mPullRefreshLayout.setOnRefreshListener(new PullRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                mPullRefreshLayout.postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        memberDatas.clear();
                        //new refreshTask(name,password).execute();
                        mPullRefreshLayout.setRefreshing(false);
                    }
                }, 3000);
            }
        });

        final View actionA = findViewById(R.id.action_a);
        actionA.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Log.d("FAB", "1 pressed");
            }
        });
        final View actionB = findViewById(R.id.action_b);
        actionB.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Log.d("FAB", "2 pressed");
            }
        });
    }

    /**
     * This method init widget in this activity.
     */
    private void init(){
        mListView = (ListView)findViewById(R.id.family_listview);
        mPullRefreshLayout = (PullRefreshLayout)findViewById(R.id.swipeRefreshLayout);
        mFabA = findViewById(R.id.action_a);
        mFabB = findViewById(R.id.action_b);
        menuMultipleActions = (FloatingActionsMenu) findViewById(R.id.multiple_actions);
    }

    /**
     * This method recive the data from LoginActivity.
     */
    private void getData(){
        Intent intent = getIntent();
        Bundle bundle = intent.getExtras();
        mUserName = bundle.getString(Canstant.NAME);
        mUserPassword = bundle.getString(Canstant.PASSWORD);
    }

    /**
     * This method set Listeners.
     */
    private void setListeners(){
        // First fab button
        mFabA.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Log.d("FAB", "1 pressed");
            }
        });
        // Second fab button
        mFabB.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Log.d("FAB", "2 pressed");
            }
        });
    }

    private void fillData(){
       // new refreshTask(name,password).execute();
        MemberData dataone = new MemberData("Jianyu","06/05/1993",true,false);
        memberDatas.add(dataone);
        MemberData datatwo = new MemberData("weiwei","09/25/1993",true,true);
        memberDatas.add(datatwo);
        //Log.d("LISTTAG","this:"+memberDatas.get(0).getName());
    }

}


//
//FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
//fab.setOnClickListener(new View.OnClickListener() {
//@Override
//public void onClick(View view) {
//        Snackbar.make(view, "Replace with your own action", Snackbar.LENGTH_LONG)
//        .setAction("Action", null).show();
//        }
//        });