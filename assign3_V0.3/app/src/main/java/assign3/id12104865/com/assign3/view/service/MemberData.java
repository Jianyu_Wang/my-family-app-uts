package assign3.id12104865.com.assign3.view.service;

/**
 * Created by Jianyu1 on 15/10/2.
 */
public class MemberData {
    private Boolean leaveMessage;
    private String name;
    private String birthday;
    private String message;

    public MemberData(String name, String birthday,Boolean leaveMessage) {
        this.leaveMessage = leaveMessage;
        this.name = name;
        this.birthday = birthday;

    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public void setLeaveMessage(Boolean leaveMessage) {
        this.leaveMessage = leaveMessage;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setBirthday(String birthday) {
        this.birthday = birthday;
    }

    public Boolean getLeaveMessage() {
        return leaveMessage;
    }

    public String getName() {
        return name;
    }

    public String getBirthday() {
        return birthday;
    }
}
