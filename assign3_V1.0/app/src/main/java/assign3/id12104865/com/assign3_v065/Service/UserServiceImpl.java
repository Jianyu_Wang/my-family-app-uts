package assign3.id12104865.com.assign3_v065.Service;


import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;

import assign3.id12104865.com.assign3_v065.Helper.CustomerHttpClientHelper;
import assign3.id12104865.com.assign3_v065.Model.Constant;

/**
 * Created by Jianyu1 on 15/10/20.
 * Implements UserService to connect to service
 *   Used IPv4:     --192.168.1.103 (home)
 *                  --192.168.1.104 (home)
 *                  --172.19.117.236 (UTS)
 *                  --172.19.152.27 (UTS)
 *                  --172.19.117.183 (UTS)
 */

public class UserServiceImpl implements UserService{

    private static final String IPv4 = "http://172.19.117.102:8080/jianyu/";

    // This method for login (check the name and password from user)
    @Override
    public int userLogin(String username, String password) throws Exception {
        String url = IPv4 + "login.do";
        // set Json
        JSONObject object = new JSONObject();
        object.put(Constant.NAME,username);
        object.put(Constant.PASSWORD,password);
        // set parameter
        NameValuePair parameter = new BasicNameValuePair(Constant.PARAMETER_LOGIN_TAG,object.toString());
        String re = CustomerHttpClientHelper.post(url, parameter);
        // get JSON
        JSONObject jsonResults = new JSONObject(re);
        int result = Integer.parseInt(jsonResults.getString(Constant.RESULT_TAG));
        if (result == 1){
            return Integer.parseInt(jsonResults.getString(Constant.CODE));
        }else{
            String errorMsg = jsonResults.getString(Constant.ERROR_TAG);
            throw new ServiceRulesException(errorMsg);
        }
        //return 0;
    }

    // This method for register (record the name and password from user)
    @Override
    public void register(String user_name, String password, String family_code, String isCreator,String birthday) throws Exception {
        String url = IPv4 + "register.do";
        // set Json
        JSONObject object = new JSONObject();
        object.put(Constant.NAME,user_name);
        object.put(Constant.PASSWORD,password);
        object.put(Constant.CODE,family_code);
        object.put(Constant.IS_CREATOR,isCreator);
        object.put(Constant.BIRTHDAY,birthday);
        // set parameter
        NameValuePair parameter = new BasicNameValuePair(Constant.PARAMETER_REGISTER_TAG,object.toString());
        String re = CustomerHttpClientHelper.post(url, parameter);
        // get JSON
        JSONObject jsonResults = new JSONObject(re);
        int result = Integer.parseInt(jsonResults.getString(Constant.RESULT_TAG));
        if (result == 1){

        }else{
            String errorMsg = jsonResults.getString(Constant.ERROR_TAG);
            throw new ServiceRulesException(errorMsg);
        }
    }

    // This method can refresh list by the new data from service
    @Override
    public String refreshFamilyList(String username, String password) throws Exception {
        String url = IPv4 + "FamilyRfresh";
        // set Json
        JSONObject object = new JSONObject();
        object.put(Constant.NAME,username);
        object.put(Constant.PASSWORD,password);
        // set parameter
        NameValuePair parameter = new BasicNameValuePair(Constant.PARAMETER_REFRESH_TAG,object.toString());
        String re = CustomerHttpClientHelper.post(url, parameter);
        return re;
    }

    @Override
    public String getWishData(String user_name, String password) throws Exception {
        String url = IPv4 + "WishData";
        // set Json
        JSONObject object = new JSONObject();
        object.put(Constant.NAME,user_name);
        object.put(Constant.PASSWORD,password);
        // set parameter
        NameValuePair parameter = new BasicNameValuePair(Constant.PARAMETER_REFRESH_TAG,object.toString());
        String re = CustomerHttpClientHelper.post(url, parameter);
        return re;
    }

    @Override
    public String getEmailData(ArrayList names) throws Exception {
        String url = IPv4 + "Email";
        // set Json
        JSONArray jsonArray = new JSONArray();
        for(int i = 0;i < names.size();i++){
            JSONObject object = new JSONObject();
            object.put(Constant.NAME, names.get(i));
            jsonArray.put(object);
        }
        // set parameter
        NameValuePair parameter = new BasicNameValuePair(Constant.PARAMETER_REFRESH_TAG,jsonArray.toString());
        String re = CustomerHttpClientHelper.post(url, parameter);
        return re;
    }

    @Override
    public void updateData(String name,String password, String email, String wish) throws Exception {
        String url = IPv4 + "Update";
        // set Json
        JSONObject object = new JSONObject();
        object.put(Constant.NAME,name);
        object.put(Constant.PASSWORD,password);
        object.put(Constant.EMAIL,email);
        object.put(Constant.WISH_TITLE, wish);
        // set parameter
        NameValuePair parameter = new BasicNameValuePair(Constant.PARAMETER_REGISTER_TAG,object.toString());
        String re = CustomerHttpClientHelper.post(url, parameter);
    }
}
