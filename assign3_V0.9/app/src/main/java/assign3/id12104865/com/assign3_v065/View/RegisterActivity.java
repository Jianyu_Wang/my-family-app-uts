package assign3.id12104865.com.assign3_v065.View;

import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.TextView;
import android.widget.Toast;

import java.util.Random;

import assign3.id12104865.com.assign3_v065.R;
import assign3.id12104865.com.assign3_v065.Service.ServiceRulesException;
import assign3.id12104865.com.assign3_v065.Service.UserService;
import assign3.id12104865.com.assign3_v065.Service.UserServiceImpl;

/**
 * RegisterActivity allowed user register for this app.
 *  User can set their name and password.
 *  User can choose join or create a family.
 *  If user wanna join a family, he/she should provide the familycode.
 */

public class RegisterActivity extends AppCompatActivity {

    private static final String IS_CREATOR = "1";
    private static final String IS_NOT_CREATOR = "0";
    private static final int EMPTY_EDIT_TEXT = 0;
    private static final int LENGTH_OF_CODE = 6;

    private EditText mUserName;
    private EditText mPassword;
    private CheckBox mCreateCheckbox;
    private CheckBox mJoinCheckbox;
    private TextView mCodeText;
    private EditText mCodeEdit;
    private ImageButton mCancelImagebutton;
    private ImageButton mRegisterImagebutton;
    private UserService mUserService = new UserServiceImpl();
    private EditText mYearEdit;
    private EditText mMothEdit;
    private EditText mDayEdit;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register);
        // init button edit text and text view.
        init();
        // set Listeners to every button and check box.
        setListeners();
    }

    /**
     * This method init the button and edittext in login activity by Id.
     */
    private void init(){
        mCreateCheckbox = (CheckBox)findViewById(R.id.checkBox_create_family_RegisterActivity);
        mCreateCheckbox.setChecked(true);
        mJoinCheckbox = (CheckBox)findViewById(R.id.checkBox_join_family_RegisterActivity);
        mCodeText = (TextView)findViewById(R.id.textView_family_code_RegisterActivity);
        mCodeEdit = (EditText)findViewById(R.id.editText_family_code_RegisterActivity);
        mCancelImagebutton = (ImageButton)findViewById(R.id.imageButton_cancel_RegisterActivity);
        mRegisterImagebutton = (ImageButton)findViewById(R.id.imageButton_register_RegisterActivity);
        mUserName = (EditText)findViewById(R.id.editText_username_RegisterActivity);
        mPassword = (EditText)findViewById(R.id.editText_password_RegisterActivity);
        mYearEdit = (EditText)findViewById(R.id.editText_year_RegisterActivity);
        mMothEdit = (EditText)findViewById(R.id.editText_moth_RegisterActivity);
        mDayEdit = (EditText)findViewById(R.id.editText_day_RegisterActivity);
    }

    /**
     *  This method set the Listeners for every button and check box.
     */
    private void setListeners(){
        // create Check box
        mCreateCheckbox.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if(mCreateCheckbox.isChecked()){
                    mJoinCheckbox.setChecked(false);
                    cleanCreAndCode();
                    goneFamilyRegisiter();
                }else{
                    mJoinCheckbox.setChecked(true);
                    visibleFamilyRegister();
                }
            }
        });
        // join Check box
        mJoinCheckbox.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if(mJoinCheckbox.isChecked()){
                    mCreateCheckbox.setChecked(false);
                    visibleFamilyRegister();
                }else{
                    mCreateCheckbox.setChecked(true);
                    cleanCreAndCode();
                    goneFamilyRegisiter();
                }
            }
        });
        // cancel button
        mCancelImagebutton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
        // register button
        mRegisterImagebutton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // get user's input
                String user_name = mUserName.getText().toString();
                String user_password = mPassword.getText().toString();
                String user_birthday = mDayEdit.getText().toString() + "-" +
                                       mMothEdit.getText().toString() + "-" +
                                       mYearEdit.getText().toString();
                // check register information and connect to servlet
                if (user_name.length() > EMPTY_EDIT_TEXT && user_password.length() > EMPTY_EDIT_TEXT) {
                    if (mCreateCheckbox.isChecked()) {
                        String fam_code = randomCode();
                        if (checkFamCode(fam_code)) {
                            new registerTask(user_name, user_password, fam_code, IS_CREATOR,user_birthday).execute();
                        } else {
                            showToast(R.string.code_create_error);
                        }
                    } else {
                        String fam_code = mCodeEdit.getText().toString();
                        if (checkFamCode(fam_code)) {
                            new registerTask(user_name, user_password, fam_code, IS_NOT_CREATOR,user_birthday).execute();
                        } else {
                            showToast(R.string.code_input_error);
                        }
                    }
                } else {
                    showToast(R.string.register_details_fill_error);
                }
            }
        });
    }

    /**
     * Show the toast
     * @param strId that want to show in screen
     */
    private void showToast(int strId){
        Toast.makeText(RegisterActivity.this, strId, Toast.LENGTH_LONG).show();
    }

    /**
     * clean family creator and family code text view.
     */
    private void cleanCreAndCode(){
        mCodeEdit.setText("");
    }

    /**
     * create random code.
     * @return a random number.
     */
    private String randomCode(){
        Random random = new Random();
        String result = "";
        for (int i = 0; i < LENGTH_OF_CODE; i++){
            result += random.nextInt(10);
        }
        return result;
    }

    /**
     *
     * @param famcode that input from user
     * @return is the famcode regular (6 number)
     */
    private boolean checkFamCode(String famcode){
        if(famcode.length() == LENGTH_OF_CODE){
            try {
                Integer.parseInt(famcode);
                return true;
            } catch (NumberFormatException e) {
                return false;
            }
        }else{
            return false;
        }
    }

    /**
     * Show code text view and edit view then user can input family code
     */
    public void visibleFamilyRegister(){
        mCodeText.setVisibility(View.VISIBLE);
        mCodeEdit.setVisibility(View.VISIBLE);
    }

    /**
     * Gone code text view and edit view then user cannot input family code
     */
    public void goneFamilyRegisiter(){
        mCodeText.setVisibility(View.GONE);
        mCodeEdit.setVisibility(View.GONE);
    }

    /**
     * Asyntask for register
     * Toast is the register success and back to loginActivity
     */
    private class registerTask extends AsyncTask<Void,Void,Void> {

        private String mUsername;
        private String mPassword;
        private String mFamilyCode;
        private String mIsCreator;
        private String mBirtday;
        private String mFamCreator;
        private int strId = R.string.success_register;

        public registerTask(String username,String password,String code,String iscreator,String birtday){
            mUsername = username;
            mPassword = password;
            mFamilyCode = code;
            mIsCreator = iscreator;
            mBirtday = birtday;
        }

        @Override
        protected Void doInBackground(Void... params) {
            // Connect to service and show error (if happen)
            try {
                mUserService.register(mUsername,mPassword,mFamilyCode,mIsCreator,mBirtday);
            } catch (ServiceRulesException e) {
                strId = R.string.register_fail;
            } catch (Exception e) {
                e.printStackTrace();
            }
            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            // After get the result from service show the result and go back to loginActivity
            finish();
            showToast(strId);
            super.onPostExecute(aVoid);
        }
    }
}
