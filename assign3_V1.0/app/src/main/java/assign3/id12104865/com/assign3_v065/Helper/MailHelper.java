package assign3.id12104865.com.assign3_v065.Helper;

import android.content.Context;
import android.content.Intent;
import android.widget.Toast;

import assign3.id12104865.com.assign3_v065.R;

/**
 * Created by Jianyu1 on 15/11/2.
 * MailHelper support to open the system mail app and fill the address and details
 */
public class MailHelper {

    private static final String MAIL = "MAIL";
    private static final String CC = "CC";
    private static final String TYPE = "plain/text";
    private static final int EMPTY_ADDRESS = 0;

    private String[] mEmailAddress;
    private Context mContext = null;
    private String mDetails;

    public MailHelper(String[] emailAddress,Context context,String details){
        mEmailAddress = emailAddress;
        mContext = context;
        mDetails = details;
    }

    /**
     *  This method starts the system mail app
     *  Fill the address and details
     */
    public int sendMailByIntent() {
        if(mEmailAddress.length != EMPTY_ADDRESS){
            Intent intent = new Intent(android.content.Intent.ACTION_SEND);
            intent.setType(TYPE);
            intent.putExtra(android.content.Intent.EXTRA_EMAIL, mEmailAddress);
            intent.putExtra(android.content.Intent.EXTRA_CC, CC);
            intent.putExtra(android.content.Intent.EXTRA_TEXT, mDetails);
            mContext.startActivity(Intent.createChooser(intent, MAIL));
        }else{
            Toast.makeText(mContext,mContext.getString(R.string.no_setting_email), Toast.LENGTH_LONG).show();
        }

        return 1;
    }
}
