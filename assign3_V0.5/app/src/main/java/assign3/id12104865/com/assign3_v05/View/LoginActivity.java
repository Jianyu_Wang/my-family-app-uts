package assign3.id12104865.com.assign3_v05.View;

import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.EditText;
import android.widget.ImageButton;

import assign3.id12104865.com.assign3_v05.R;

public class LoginActivity extends AppCompatActivity {

    private ImageButton mRegister_imagebutton;
    private ImageButton mLogin_imagebutton;
    private EditText mUser_name;
    private EditText mPassword;
    //private UserService mUserService = new UserServiceImpl();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        // init button and edit text.
        init();
        // set Listeners to every button
        setListeners();
    }

    /**
     * This method init the button and edittext in login activity by Id.
     */
    private void init(){
        mRegister_imagebutton = (ImageButton)findViewById(R.id.imagebutton_register_LoginActivity);
        mLogin_imagebutton = (ImageButton)findViewById(R.id.imageButton_login_LoginActivity);
        mUser_name = (EditText)findViewById(R.id.editText_login_name_LoginActivity);
        mPassword = (EditText)findViewById(R.id.editText_login_password_LoginActivity);
    }

    /**
     *  This method set the Listeners for every button
     */
    private void setListeners(){
        // register button clicked
        mRegister_imagebutton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent();
                intent.setClass(LoginActivity.this, RegisterActivity.class);
                startActivity(intent);
            }
        });

        // login button clicked
        mLogin_imagebutton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
//                String user_name = mUser_name.getText().toString();
//                String user_password = mPassword.getText().toString();
//                new loginTask(user_name,user_password).execute();
//                Intent intent = new Intent();
//                intent.setClass(LoginActivity.this, chatActivity.class);
//                intent.putExtra(Constants.USER_NAME,mUser_name.getText().toString());
//                intent.putExtra(Constants.PASSWORD,mPassword.getText().toString());
//                startActivityForResult(intent,1);
            }
        });
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_login, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
}
