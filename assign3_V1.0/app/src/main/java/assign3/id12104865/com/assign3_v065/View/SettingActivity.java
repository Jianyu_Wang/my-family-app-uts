package assign3.id12104865.com.assign3_v065.View;

import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

import com.github.florent37.materialtextfield.MaterialTextField;

import assign3.id12104865.com.assign3_v065.Model.Constant;
import assign3.id12104865.com.assign3_v065.R;
import assign3.id12104865.com.assign3_v065.Service.UserService;
import assign3.id12104865.com.assign3_v065.Service.UserServiceImpl;


/**
 * SettingActivity allowed user change their password,email and wish.
 */
public class SettingActivity extends AppCompatActivity {

    private FloatingActionButton mFabBack;
    private FloatingActionButton mFabUpdate;
    private MaterialTextField mPasswordEdit;
    private MaterialTextField mEmailEdit;
    private MaterialTextField mWishEdit;
    private String mUserName;
    private UserService mUserService = new UserServiceImpl();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_setting);
        // get data from last activity
        getData();
        // init widget
        init();
        // set Listener
        setListener();
    }

    // init widget
    private void init() {
        mFabBack = (FloatingActionButton)findViewById(R.id.fab_back_setting_activity);
        mFabUpdate = (FloatingActionButton)findViewById(R.id.fab_update_setting_activity);
        mPasswordEdit = (MaterialTextField)findViewById(R.id.editText_password_setting_activity);
        mEmailEdit = (MaterialTextField)findViewById(R.id.editText_email_setting_activity);
        mWishEdit = (MaterialTextField)findViewById(R.id.editText_wish_setting_activity);
    }

    /**
     * This method recive the data from LoginActivity.
     */
    private void getData(){
        Intent intent = getIntent();
        Bundle bundle = intent.getExtras();
        mUserName = bundle.getString(Constant.NAME);
    }

    /**
     * This method set listener for fab button
     */
    private void setListener(){
        // The back button pressed
        mFabBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
        // The set button pressed
        mFabUpdate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String password = checkEmpty(mPasswordEdit.getEditText());
                String email = checkEmpty(mEmailEdit.getEditText());
                String wish = checkEmpty(mWishEdit.getEditText());
                new updateData(mUserName,password,email,wish).execute();
            }
        });
    }

    private String checkEmpty(EditText text){
        if(text.length() == 0){
            return "0";
        }else{
            return text.getText().toString();
        }
    }

    /**
     * updateData update data to service
     */
    private class updateData extends AsyncTask<Void, Void, Void> {
        private String mUsername;
        private String mPassword;
        private String mEmail;
        private String mWish;
        String re;
        private boolean success = true;

        public updateData(String username,String password,String email,String wish) {
            mUsername = username;
            mPassword = password;
            mEmail = email;
            mWish = wish;
        }

        @Override
        protected Void doInBackground(Void... params) {
            try {
                mUserService.updateData(mUsername,mPassword,mEmail,mWish);
            } catch (Exception e) {
                success = false;
            }
            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);
            if(!success){
                Toast.makeText(SettingActivity.this, getString(R.string.update_fail), Toast.LENGTH_LONG).show();
            }
            finish();
        }
    }

}
