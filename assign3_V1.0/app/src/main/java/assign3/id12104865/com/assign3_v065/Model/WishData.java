package assign3.id12104865.com.assign3_v065.Model;

/**
 * Created by Jianyu1 on 15/10/31.
 */
public class WishData {
    private String name;
    private String wish;

    public WishData(String name, String wish) {
        this.name = name;
        this.wish = wish;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getWish() {
        return wish;
    }
}
