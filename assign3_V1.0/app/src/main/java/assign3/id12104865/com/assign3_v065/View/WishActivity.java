package assign3.id12104865.com.assign3_v065.View;

import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.util.TypedValue;
import android.view.View;

import com.astuetz.PagerSlidingTabStrip;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.concurrent.ExecutionException;

import assign3.id12104865.com.assign3_v065.Adapter.WishPagerAdapter;
import assign3.id12104865.com.assign3_v065.Model.Constant;
import assign3.id12104865.com.assign3_v065.Model.WishData;
import assign3.id12104865.com.assign3_v065.R;
import assign3.id12104865.com.assign3_v065.Service.ServiceRulesException;
import assign3.id12104865.com.assign3_v065.Service.UserService;
import assign3.id12104865.com.assign3_v065.Service.UserServiceImpl;

/**
 * WishActivity show the list of family members' wish
 */
public class WishActivity extends AppCompatActivity {

    private ViewPager mPager;
    private PagerSlidingTabStrip mTabs;
    private WishPagerAdapter adapter;
    private String mUserName;
    private String mUserPassword;
    private FloatingActionButton mFabBack;
    private FloatingActionButton mFabSet;
    private UserService mUserService = new UserServiceImpl();
    private ArrayList<WishData> wishDatas = new ArrayList<WishData>();
    //private SystemBarTintManager

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_wish);
        // get data from last activity
        getData();
        // init widget
        init();
        // set Listener
        setListener();
    }

    // init widget
    private void init() {
        mPager = (ViewPager)findViewById(R.id.pager_WishActivity);
        mTabs = (PagerSlidingTabStrip)findViewById(R.id.tabs_WishActivity);
        // init adapter and card view
        adapter = new WishPagerAdapter(getSupportFragmentManager(),wishDatas);
        mPager.setAdapter(adapter);
        mTabs.setViewPager(mPager);
        final int pageMargin = (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, 4, getResources()
                .getDisplayMetrics());
        mPager.setPageMargin(pageMargin);
        mPager.setCurrentItem(1);
        mFabBack = (FloatingActionButton)findViewById(R.id.fab_back_wish_activity);
        mFabSet = (FloatingActionButton)findViewById(R.id.fab_set_wish_activity);
    }

    /**
     * This method set listener for fab button
     */
    private void setListener(){
        // The back button pressed
        mFabBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
        // The set button pressed
        mFabSet.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent();
                intent.setClass(WishActivity.this, SettingActivity.class);
                intent.putExtra(Constant.NAME, mUserName);
                startActivityForResult(intent, 1);
            }
        });
    }

    /**
     * This method recive the data from LoginActivity.
     */
    private void getData(){
        Intent intent = getIntent();
        Bundle bundle = intent.getExtras();
        mUserName = bundle.getString(Constant.NAME);
        mUserPassword = bundle.getString(Constant.PASSWORD);
        try {
            wishDatas = new loadWishData(mUserName,mUserPassword).execute().get();
        } catch (InterruptedException e) {
            e.printStackTrace();
        } catch (ExecutionException e) {
            e.printStackTrace();
        }

    }

    /**
     * loadWishData load wish data from service
     */
    private class loadWishData extends AsyncTask<Void, Void, ArrayList> {
        private String mUsername;
        private String mPassword;
        private String str = getString(R.string.success_login);
        String re;
        private boolean success = true;

        public loadWishData(String username, String password) {
            mUsername = username;
            mPassword = password;
        }

        @Override
        protected ArrayList doInBackground(Void... params) {
            //check the input from users.
            try {
                re = mUserService.getWishData(mUsername, mPassword);
                return readJson(re);
            } catch (ServiceRulesException e) {
                success = false;
                str = e.toString();
            } catch (Exception e) {
                success = false;
                str = e.toString();
            }
            return readJson(re);
        }

        private ArrayList readJson(String re) {
            ArrayList<WishData> datasTemp = new ArrayList<WishData>();
            JSONArray array = null;
            try {
                array = new JSONArray(re);
                for (int i = 0; i < array.length(); i++) {
                    WishData data;
                    JSONObject obj = array.getJSONObject(i);
                    String name = obj.getString(Constant.NAME);
                    String wish = obj.getString(Constant.WISH_TITLE);
                    data = new WishData(name, wish);
                    datasTemp.add(data);

                }
            } catch (JSONException e) {
                e.printStackTrace();
            }
            return datasTemp;
        }
    }
}