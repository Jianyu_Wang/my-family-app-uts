package assign3.id12104865.com.assign3.view.view;

import android.os.AsyncTask;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.TextView;
import android.widget.Toast;

import java.util.Random;

import assign3.id12104865.com.assign3.R;
import assign3.id12104865.com.assign3.view.service.ServiceRulesException;
import assign3.id12104865.com.assign3.view.service.UserService;
import assign3.id12104865.com.assign3.view.service.UserServiceImpl;


public class RegisterActivity extends ActionBarActivity {

    private static final String IS_CREATOR = "1";
    private static final String IS_NOT_CREATOR = "0";
    private static final int EMPTY_EDIT_TEXT = 0;
    private static final int LENGTH_OF_CODE = 6;
    private EditText mUser_name;
    private EditText mPassword;
    private CheckBox mCreate_checkbox;
    private CheckBox mJoin_checkbox;
    private TextView mCreate_text;
    private EditText mCreate_edit;
    private TextView mCode_text;
    private EditText mCode_edit;
    private ImageButton mCancel_imagebutton;
    private ImageButton mRegister_imagebutton;
    private UserService mUserService;

    public void init(){
        mCreate_checkbox = (CheckBox)findViewById(R.id.checkBox_create_family_register_activity);
        mCreate_checkbox.setChecked(true);
        mJoin_checkbox = (CheckBox)findViewById(R.id.checkBox_join_family_register_activity);
        mCreate_text = (TextView)findViewById(R.id.textView_family_creator_register_activity);
        mCreate_edit = (EditText)findViewById(R.id.editText_family_creator_register_activity);
        mCode_text = (TextView)findViewById(R.id.textView_family_code_register_activity);
        mCode_edit = (EditText)findViewById(R.id.editText_family_code_register_activity);
        mCancel_imagebutton = (ImageButton)findViewById(R.id.imageButton_cancel_register_activity);
        mRegister_imagebutton = (ImageButton)findViewById(R.id.imageButton_register_register_activity);
        mUser_name = (EditText)findViewById(R.id.editText_username_register_activity);
        mPassword = (EditText)findViewById(R.id.editText_password_register_activity);
        mUserService = new UserServiceImpl();
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register);
        // init
        init();

        // create Check box
        mCreate_checkbox.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if(mCreate_checkbox.isChecked()){
                    mJoin_checkbox.setChecked(false);
                    cleanCreAndCode();
                    goneFamilyRegisiter();
                }else{
                    mJoin_checkbox.setChecked(true);
                    visibleFamilyRegister();
                }
            }
        });
        // join Check box
        mJoin_checkbox.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if(mJoin_checkbox.isChecked()){
                    mCreate_checkbox.setChecked(false);
                    visibleFamilyRegister();
                }else{
                    mCreate_checkbox.setChecked(true);
                    cleanCreAndCode();
                    goneFamilyRegisiter();
                }
            }
        });

        // cancel button
        mCancel_imagebutton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

        // register button
        mRegister_imagebutton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String user_name = mUser_name.getText().toString();
                String user_password = mPassword.getText().toString();
                // check register information and connect to servlet
                if (user_name.length() > EMPTY_EDIT_TEXT && user_password.length() > EMPTY_EDIT_TEXT) {
                    if (mCreate_checkbox.isChecked()) {
                        String fam_code = randomCode();
                        String fam_cre = user_name;
                        if (checkFamCode(fam_code)) {
                            new registerTask(user_name, user_password, fam_code, IS_CREATOR, fam_cre).execute();
                        } else {
                            Toast.makeText(RegisterActivity.this, R.string.code_create_error, Toast.LENGTH_LONG).show();
                        }
                    } else {
                        String fam_code = mCode_edit.getText().toString();
                        String fam_cre = mCreate_edit.getText().toString();
                        if (checkFamCode(fam_code) && fam_cre.length() != EMPTY_EDIT_TEXT) {
                            new registerTask(user_name, user_password, fam_code, IS_NOT_CREATOR, fam_cre).execute();
                        } else {
                            Toast.makeText(RegisterActivity.this, R.string.code_input_error, Toast.LENGTH_LONG).show();
                        }
                    }
                } else {
                    Toast.makeText(RegisterActivity.this, R.string.register_details_fill_error, Toast.LENGTH_LONG).show();
                }
            }
        });
    }

    // clean family creator and family code
    private void cleanCreAndCode(){
        mCode_edit.setText("");
        mCreate_edit.setText("");
    }

    // create random code
    private String randomCode(){
        Random random = new Random();
        String result = "";
        for (int i = 0; i < LENGTH_OF_CODE; i++){
            result += random.nextInt(10);
        }
        return result;
    }

    private boolean checkFamCode(String famcode){
        if(famcode.length() == LENGTH_OF_CODE){
            try {
                Integer.parseInt(famcode);
                return true;
            } catch (NumberFormatException e) {
                return false;
            }
        }else{
            return false;
        }
    }



    public void visibleFamilyRegister(){
        mCreate_text.setVisibility(View.VISIBLE);
        mCreate_edit.setVisibility(View.VISIBLE);
        mCode_text.setVisibility(View.VISIBLE);
        mCode_edit.setVisibility(View.VISIBLE);
    }

    public void goneFamilyRegisiter(){
        mCreate_text.setVisibility(View.GONE);
        mCreate_edit.setVisibility(View.GONE);
        mCode_text.setVisibility(View.GONE);
        mCode_edit.setVisibility(View.GONE);
    }

    // asyntask for register
    private class registerTask extends AsyncTask<Void,Void,Void> {

        private String mUsername;
        private String mPassword;
        private String mFamilyCode;
        private String mIsCreator;
        private String mFamCreator;
        private String str = getString(R.string.success_register);

        public registerTask(String username,String password,String code,String iscreator,String creator){
            mUsername = username;
            mPassword = password;
            mFamilyCode = code;
            mIsCreator = iscreator;
            mFamCreator = creator;
        }

        @Override
        protected Void doInBackground(Void... params) {
            try {
                mUserService.register(mUsername,mPassword,mFamilyCode,mIsCreator,mFamCreator);
            } catch (ServiceRulesException e) {
                str = e.toString();
            } catch (Exception e) {
                str = e.toString();
            }
            return null;
        }

        @Override
        protected void onProgressUpdate(Void... values) {
            super.onProgressUpdate(values);
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            finish();
            Toast.makeText(RegisterActivity.this, str, Toast.LENGTH_LONG).show();
            super.onPostExecute(aVoid);
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_register, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
}
