package assign3.id12104865.com.assign3_v065.Adapter;

import android.annotation.TargetApi;
import android.content.Context;
import android.os.Build;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.TextView;

import java.util.ArrayList;

import assign3.id12104865.com.assign3_v065.Model.MemberData;
import assign3.id12104865.com.assign3_v065.R;

/**
 * Created by Jianyu1 on 15/10/20.
 */
public class MemberAdapter extends BaseAdapter {

    private LayoutInflater mInflater;
    private Context mContext = null;
    private ArrayList<MemberData> mMemberDataArrayList;


    public MemberAdapter(Context context, ArrayList memberlist){
        this.mContext = context;
        this.mMemberDataArrayList = memberlist;
        mInflater = LayoutInflater.from(context);
    }

    @Override
    public int getCount() {
        return mMemberDataArrayList.size();
    }

    @Override
    public Object getItem(int position) {
        return mMemberDataArrayList.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @TargetApi(Build.VERSION_CODES.JELLY_BEAN)
    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View view;
        final ViewHolder holder;

        if (convertView == null) {
            view = mInflater.inflate(R.layout.list_item,parent,false);
            holder = new ViewHolder();

            holder.name = (TextView)view.findViewById(R.id.textView_name);
            holder.birthday = (TextView)view.findViewById(R.id.textView_birsthday);
            holder.emailBox = (CheckBox)view.findViewById(R.id.checkbox_newmessage_listItem);
            view.setTag(holder);
        }else{
            view = convertView;
            holder = (ViewHolder)view.getTag();
        }


        final MemberData memberData = mMemberDataArrayList.get(position);

        holder.name.setText(memberData.getmName());
        holder.birthday.setText(memberData.getmBirthday());
        if (memberData.ismIsNewMessage()){
            Log.d("Adapter","VISIBLE");
            holder.emailBox.setVisibility(View.VISIBLE);
        }else{
            Log.d("Adapter","INVISIBLE");
            holder.emailBox.setVisibility(View.INVISIBLE);
        }
        holder.emailBox.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if(holder.emailBox.isChecked()){
                    memberData.setmIsEmailChecked(true);
                }else{
                    memberData.setmIsEmailChecked(false);
                }
            }
        });
        return view;
    }


//        private class clickEvents implements View.OnClickListener{
//
//            ViewHolder mHolder;
//            TrainData mTrainData;
//
//            public clickEvents(TrainData trainData,ViewHolder holder){
//                mHolder = holder;
//                mTrainData = trainData;
//            }
//
//            @Override
//            public void onClick(View v) {
//                int randomNumber = ((int) (Math.random() * 10 + 1));
//                mTrainData.setArrivalTime(mTrainData.getArrivalTime() - randomNumber);
//                new updateData(mHolder).execute();
//
//            }
//        }


    private class ViewHolder{
        public TextView name;
        public TextView birthday;
        public CheckBox emailBox;
    }
}
