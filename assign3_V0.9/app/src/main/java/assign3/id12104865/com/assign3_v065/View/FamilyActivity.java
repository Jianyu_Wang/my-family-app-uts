package assign3.id12104865.com.assign3_v065.View;

import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.ListView;
import android.widget.Toast;

import com.baoyz.widget.PullRefreshLayout;
import com.getbase.floatingactionbutton.FloatingActionsMenu;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import assign3.id12104865.com.assign3_v065.Adapter.MemberAdapter;
import assign3.id12104865.com.assign3_v065.Model.Constant;
import assign3.id12104865.com.assign3_v065.Model.MemberData;
import assign3.id12104865.com.assign3_v065.R;
import assign3.id12104865.com.assign3_v065.Service.UserService;
import assign3.id12104865.com.assign3_v065.Service.UserServiceImpl;

public class FamilyActivity extends AppCompatActivity {

    private ListView mListView;
    private FloatingActionButton mFabBack;
    private ArrayList<MemberData> memberDatas = new ArrayList<MemberData>();
    private MemberAdapter mAdapter;
    private String mUserName;
    private String mUserPassword;
    private PullRefreshLayout mPullRefreshLayout;
    private UserService mUserService = new UserServiceImpl();
    private View mFabWishList;
    private View mFabSetting;
    private FloatingActionsMenu menuMultipleActions;
    private FloatingActionButton mFabEmail;
    private FloatingActionButton mFabSend;
    private boolean isEmailButtonClick = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_family);
        // init the widgets
        init();
        // get data from last activity
        getData();
        // fill the list data
        fillData();
        // set Listener to button
        setListeners();
        // create new adapter
        mAdapter = new MemberAdapter(this,memberDatas);
        // set adapter for list
        mListView.setAdapter(mAdapter);
        // set the PullRefreshList
        mPullRefreshLayout.setOnRefreshListener(new PullRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                mPullRefreshLayout.postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        memberDatas.clear();
                        new RefreshTask(mUserName, mUserPassword).execute();
                        mPullRefreshLayout.setRefreshing(false);
                    }
                }, 3000);
            }
        });

    }

    @Override
    protected void onRestart() {
        super.onRestart();
        changeEmailButton(false);
        mFabSend.setVisibility(View.INVISIBLE);
        isEmailButtonClick = false;
    }

    /**
     * This method init widget in this activity.
     */
    private void init(){
        mListView = (ListView)findViewById(R.id.family_listview);
        mPullRefreshLayout = (PullRefreshLayout)findViewById(R.id.swipeRefreshLayout);
        mFabWishList = findViewById(R.id.action_wish_list_family_Activity);
        mFabSetting = findViewById(R.id.action_setting_family_Activity);
        menuMultipleActions = (FloatingActionsMenu) findViewById(R.id.multiple_actions);
        mFabEmail = (FloatingActionButton)findViewById(R.id.fab_family_activity);
        mFabSend = (FloatingActionButton)findViewById(R.id.fabSend_family_activity);
    }

    /**
     * This method recive the data from LoginActivity.
     */
    private void getData(){
        Intent intent = getIntent();
        Bundle bundle = intent.getExtras();
        mUserName = bundle.getString(Constant.NAME);
        mUserPassword = bundle.getString(Constant.PASSWORD);
    }

    /**
     * This method set Listeners.
     */
    private void setListeners(){
        // First fab button
        mFabWishList.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent();
                intent.setClass(FamilyActivity.this, WishActivity.class);
                intent.putExtra(Constant.NAME, mUserName);
                intent.putExtra(Constant.PASSWORD, mUserPassword);
                startActivityForResult(intent, 1);
            }
        });
        // Second fab button
        mFabSetting.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent();
                intent.setClass(FamilyActivity.this, SettingActivity.class);
                intent.putExtra(Constant.NAME, mUserName);
                startActivityForResult(intent, 1);
            }
        });
        // Email fab button
        mFabEmail.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(!isEmailButtonClick){
                    changeEmailButton(true);
                    isEmailButtonClick = true;
                    mFabSend.setVisibility(View.VISIBLE);
                }else{
                    changeEmailButton(false);
                    isEmailButtonClick = false;
                    mFabSend.setVisibility(View.INVISIBLE);
                }
            }
        });
        // Send fab button
        mFabSend.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ArrayList<String> names = new ArrayList<String>();
                for(int i = 0;i < memberDatas.size();i++){
                    if(memberDatas.get(i).ismIsEmailChecked()){
                        names.add(memberDatas.get(i).getmName());
                    }
                }
                if(names.size() > 0){
                    Intent intent = new Intent();
                    intent.setClass(FamilyActivity.this, MailActivity.class);
                    intent.putStringArrayListExtra(Constant.MEMBER_NAMES, names);
                    startActivityForResult(intent, 1);
                }else{
                    Toast.makeText(FamilyActivity.this, getString(R.string.pleas_choose_one_member), Toast.LENGTH_LONG).show();
                }
            }
        });
    }

    // SHOW or CLOSE the email button for members
    private void changeEmailButton(boolean state){
        for(int i = 0;i < memberDatas.size();i++){
            memberDatas.get(i).setmIsNewMessage(state);
            mAdapter.notifyDataSetChanged();
        }
    }

    /**
     * Fill the members data from service
     */
    private void fillData(){
        new RefreshTask(mUserName,mUserPassword).execute();
    }

    /**
     * Called when refresh
     */
    private class RefreshTask extends AsyncTask<Void,Void,Void> {
        private String mUsername;
        private String mPassword;
        private String result;
        private boolean mIsWish;
        private boolean mIsMessage;
        //private String str = getString(R.string.success_login);
        //private boolean success = true;

        public RefreshTask(String username, String password) {
            mUsername = username;
            mPassword = password;
        }

        @Override
        protected Void doInBackground(Void... params) {
            try {
                result = mUserService.refreshFamilyList(mUsername, mPassword);
                Log.d("JASON",result);
                readJson(result);
                publishProgress();
            } catch (Exception e) {

            }
            return null;
        }

        private void readJson(String re) {
            JSONArray array = null;
            try {
                array = new JSONArray(re);
                for (int i = 0; i < array.length(); i++) {
                    MemberData data;
                    JSONObject obj = array.getJSONObject(i);
                    String name = obj.getString(Constant.MEMBER_NAME);
                    String birthday = obj.getString(Constant.BIRTHDAY);
                    String message = obj.getString(Constant.MESSAGE);
                    data = new MemberData(name, birthday);

                    memberDatas.add(data);
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }

        @Override
        protected void onProgressUpdate(Void... values) {
            super.onProgressUpdate(values);
            mAdapter.notifyDataSetChanged();
        }
    }
}


//
//FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
//fab.setOnClickListener(new View.OnClickListener() {
//@Override
//public void onClick(View view) {
//        Snackbar.make(view, "Replace with your own action", Snackbar.LENGTH_LONG)
//        .setAction("Action", null).show();
//        }
//        });