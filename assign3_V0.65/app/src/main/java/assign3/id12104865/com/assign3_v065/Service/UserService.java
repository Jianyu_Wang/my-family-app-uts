package assign3.id12104865.com.assign3_v065.Service;

/**
 * Created by Jianyu1 on 15/10/20.
 * The interface for local connect to service
 */
public interface UserService {
    // This method for login (check the name and password from user)
    public int userLogin(String username, String password) throws Exception;
    // This method for register (record the name and password from user)
    public void register(String username,String password,String family_code,String isCreator) throws Exception;
    // This method can refresh list by the new data from service
    public String refreshFamilyList(String user_name,String password) throws Exception;
}

