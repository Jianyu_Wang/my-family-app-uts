package assign3.id12104865.com.assign3_v065.Service;


import android.util.Log;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONObject;

import assign3.id12104865.com.assign3_v065.Helper.CustomerHttpClientHelper;
import assign3.id12104865.com.assign3_v065.Model.Canstant;

/**
 * Created by Jianyu1 on 15/10/20.
 * Implements UserService to connect to service
 *   Used IPv4:     --192.168.1.103 (home)
 *                  --192.168.1.104 (home)
 *                  --172.19.117.236 (UTS)
 *                  --172.19.152.27 (UTS)
 */

public class UserServiceImpl implements UserService{

    private static final String IPv4 = "http://172.19.152.27:8080/jianyu/";
    private static final String PARAMETER_LOGIN_TAG = "logindata";
    private static final String PARAMETER_REGISTER_TAG = "data";
    private static final String PARAMETER_REFRESH_TAG = "familydata";
    private static final String RESULT_TAG = "result";
    private static final String ERROR_TAG = "errorMsg";

    // This method for login (check the name and password from user)
    @Override
    public int userLogin(String username, String password) throws Exception {
        String url = IPv4 + "login.do";
        // set Json
        JSONObject object = new JSONObject();
        object.put(Canstant.NAME,username);
        object.put(Canstant.PASSWORD,password);
        // set parameter
        NameValuePair parameter = new BasicNameValuePair(PARAMETER_LOGIN_TAG,object.toString());
        String re = CustomerHttpClientHelper.post(url, parameter);
        // get JSON
        JSONObject jsonResults = new JSONObject(re);
        int result = Integer.parseInt(jsonResults.getString(RESULT_TAG));
        if (result == 1){
            return result;
        }else{
            String errorMsg = jsonResults.getString(ERROR_TAG);
            throw new ServiceRulesException(errorMsg);
        }
        //return 0;
    }

    // This method for register (record the name and password from user)
    @Override
    public void register(String user_name, String password, String family_code, String isCreator) throws Exception {
        String url = IPv4 + "register.do";
        // set Json
        JSONObject object = new JSONObject();
        object.put(Canstant.NAME,user_name);
        object.put(Canstant.PASSWORD,password);
        object.put(Canstant.CODE,family_code);
        object.put(Canstant.IS_CREATOR,isCreator);
        // set parameter
        NameValuePair parameter = new BasicNameValuePair(PARAMETER_REGISTER_TAG,object.toString());
        String re = CustomerHttpClientHelper.post(url, parameter);
        // get JSON
        JSONObject jsonResults = new JSONObject(re);
        int result = Integer.parseInt(jsonResults.getString(RESULT_TAG));
        if (result == 1){

        }else{
            String errorMsg = jsonResults.getString(ERROR_TAG);
            throw new ServiceRulesException(errorMsg);
        }
    }

    // This method can refresh list by the new data from service
    @Override
    public String refreshFamilyList(String username, String password) throws Exception {
        String url = IPv4 + "FamilyRfresh";
        // set Json
        JSONObject object = new JSONObject();
        object.put(Canstant.NAME,username);
        object.put(Canstant.PASSWORD,password);
        // set parameter
        NameValuePair parameter = new BasicNameValuePair(PARAMETER_REFRESH_TAG,object.toString());
        String re = CustomerHttpClientHelper.post(url, parameter);
        return re;
    }
}
