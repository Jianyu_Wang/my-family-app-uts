package assign3.id12104865.com.assign3.view.service;

import android.util.Log;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.HttpStatus;
import org.apache.http.NameValuePair;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.conn.ClientConnectionManager;
import org.apache.http.conn.scheme.PlainSocketFactory;
import org.apache.http.conn.scheme.Scheme;
import org.apache.http.conn.scheme.SchemeRegistry;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.impl.conn.tsccm.ThreadSafeClientConnManager;
import org.apache.http.params.BasicHttpParams;
import org.apache.http.params.HttpConnectionParams;
import org.apache.http.params.HttpParams;
import org.apache.http.params.HttpProtocolParams;
import org.apache.http.protocol.HTTP;
import org.apache.http.util.EntityUtils;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by Jianyu1 on 15/9/26.
 */
public class CustomerHttpClient {
    private static final String TAG ="CustomerHttpClient";
    private static final String APPYL_EEROR = "Apply fail";
    private static final String CHARSET = HTTP.UTF_8;
    private static HttpClient customerHttpClient;

    private CustomerHttpClient(){}

    public static synchronized HttpClient getHttpClient(){

        if (null == customerHttpClient){
            HttpParams params = new BasicHttpParams();
            // set params
            HttpProtocolParams.setContentCharset(params, HTTP.UTF_8);
            HttpConnectionParams.setConnectionTimeout(params, 5000);
            HttpConnectionParams.setSoTimeout(params, 5000);
            SchemeRegistry schemeRegistry = new SchemeRegistry();
            schemeRegistry.register(new Scheme("http", PlainSocketFactory.getSocketFactory(), 80));
            schemeRegistry.register(new Scheme("https", PlainSocketFactory.getSocketFactory(), 433));
            // safe thread
            ClientConnectionManager conman = new ThreadSafeClientConnManager(params,schemeRegistry);
            customerHttpClient = new DefaultHttpClient(conman,params);
        }
        return customerHttpClient;
    }

    public static String post(String url, NameValuePair... pair){
        try{
            List<NameValuePair> params = new ArrayList<NameValuePair>();
            params.add(pair[0]);
            HttpPost post = new HttpPost(url);
            post.setEntity(new UrlEncodedFormEntity(params, HTTP.UTF_8));
            HttpClient client = getHttpClient();
            HttpResponse response = client.execute(post);
            if(response.getStatusLine().getStatusCode() != HttpStatus.SC_OK) {
                throw new RuntimeException(APPYL_EEROR);
            }
            //return response;
            String result = EntityUtils.toString(response.getEntity(),HTTP.UTF_8);
            Log.d("MMY",result);
            return result;
        } catch (Exception e) {
            throw new RuntimeException(APPYL_EEROR,e);
        }
    }


}
