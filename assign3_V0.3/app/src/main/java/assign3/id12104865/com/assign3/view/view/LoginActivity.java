package assign3.id12104865.com.assign3.view.view;

import android.content.Intent;
import android.os.AsyncTask;
import android.provider.SyncStateContract;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.Toast;

import assign3.id12104865.com.assign3.R;
import assign3.id12104865.com.assign3.view.service.Constants;
import assign3.id12104865.com.assign3.view.service.ServiceRulesException;
import assign3.id12104865.com.assign3.view.service.UserService;
import assign3.id12104865.com.assign3.view.service.UserServiceImpl;


public class LoginActivity extends ActionBarActivity {

    private ImageButton mRegister_imagebutton;
    private ImageButton mLogin_imagebutton;
    private EditText mUser_name;
    private EditText mPassword;
    private UserService mUserService = new UserServiceImpl();


    private void init(){
        mRegister_imagebutton = (ImageButton)findViewById(R.id.imageButton_register);
        mLogin_imagebutton = (ImageButton)findViewById(R.id.imageButton_login);
        mUser_name = (EditText)findViewById(R.id.editText_username);
        mPassword = (EditText)findViewById(R.id.editText_password);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        init();

        // register button clicked
        mRegister_imagebutton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent();
                intent.setClass(LoginActivity.this, RegisterActivity.class);
                startActivity(intent);
            }
        });

        // login button clicked
        mLogin_imagebutton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String user_name = mUser_name.getText().toString();
                String user_password = mPassword.getText().toString();
                new loginTask(user_name,user_password).execute();
//                Intent intent = new Intent();
//                intent.setClass(LoginActivity.this, chatActivity.class);
//                intent.putExtra(Constants.USER_NAME,mUser_name.getText().toString());
//                intent.putExtra(Constants.PASSWORD,mPassword.getText().toString());
//                startActivityForResult(intent,1);
            }
        });

    }

    private class loginTask extends AsyncTask<Void,Void,Void>{
        private String mUsername;
        private String mPassword;
        private String str = getString(R.string.success_login);
        private boolean success = true;

        public loginTask(String username,String password){
            mUsername = username;
            mPassword = password;
        }

        @Override
        protected Void doInBackground(Void... params) {
            try {
                mUserService.userLogin(mUsername,mPassword);
            } catch (ServiceRulesException e) {
                success = false;
                str = e.toString();
            } catch (Exception e) {
                success = false;
                str = e.toString();
            }
            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            Toast.makeText(LoginActivity.this, str, Toast.LENGTH_LONG).show();
            if (success){
                Intent intent = new Intent();
                intent.setClass(LoginActivity.this, FamilyActivity.class);
                intent.putExtra(Constants.USER_NAME, mUsername);
                intent.putExtra(Constants.PASSWORD,mPassword);
                Log.d("TAGG",mUsername);
                startActivityForResult(intent, 1);
            }
            super.onPostExecute(aVoid);
        }
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_login, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
}
