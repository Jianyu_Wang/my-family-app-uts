package assign3.id12104865.com.assign3_v065.Adapter;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.util.Log;

import java.util.ArrayList;

import assign3.id12104865.com.assign3_v065.Helper.SuperAwesomeCardFragment;
import assign3.id12104865.com.assign3_v065.Model.WishData;

/**
 * Created by Jianyu1 on 15/10/30.
 */
public class WishPagerAdapter extends FragmentPagerAdapter {

    private String[] TITLES;
    private String[] ERROR = {"no data"};
    private ArrayList<WishData> mWishDatas = new ArrayList<>();


    public WishPagerAdapter(FragmentManager fm,ArrayList wishDatas) {
        super(fm);
        mWishDatas = wishDatas;
        Log.d("PAGE", String.valueOf(wishDatas.size()));
        TITLES = new String[wishDatas.size()];
        initTITLES();
    }

    private void initTITLES(){
        for(int i = 0; i < mWishDatas.size();i++){
            TITLES[i] = mWishDatas.get(i).getName();
            Log.d("ABSBS","123");
        }
    }

    @Override
    public CharSequence getPageTitle(int position) {
        Log.d("PAGE",TITLES[position]);
        return TITLES[position];
    }

    @Override
    public int getCount() {
        return TITLES.length;
    }

    @Override
    public Fragment getItem(int position) {
        SuperAwesomeCardFragment fr = new SuperAwesomeCardFragment(position,mWishDatas.get(position).getWish());
        return  fr;
    }
}