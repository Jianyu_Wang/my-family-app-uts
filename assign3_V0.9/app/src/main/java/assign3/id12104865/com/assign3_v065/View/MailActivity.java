package assign3.id12104865.com.assign3_v065.View;

import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.concurrent.ExecutionException;

import assign3.id12104865.com.assign3_v065.Model.Constant;
import assign3.id12104865.com.assign3_v065.Model.WishData;
import assign3.id12104865.com.assign3_v065.R;
import assign3.id12104865.com.assign3_v065.Service.UserService;
import assign3.id12104865.com.assign3_v065.Service.UserServiceImpl;

public class MailActivity extends AppCompatActivity {

    private ArrayList<String> mNames;
    private String[] mEmailAddress;
    private String mEmailBody;
    private EditText mMailText;
    private UserService mUserService = new UserServiceImpl();


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_mail);

        // get data from last activity
        getData();
        // init the widgets
        init();

        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                sendMailByIntent();
            }
        });
    }

    /**
     * This method recive the data from LoginActivity.
     */
    private void getData(){
        Intent intent = getIntent();
        Bundle bundle = intent.getExtras();
        mNames = bundle.getStringArrayList(Constant.MEMBER_NAMES);
        try {
            ArrayList<String> mEmails = new loadEmailData().execute().get();
            setEmails(mEmails);
        } catch (InterruptedException e) {
            e.printStackTrace();
        } catch (ExecutionException e) {
            e.printStackTrace();
        }
    }

    /**
     * This method init widget in this activity.
     */
    private void init(){
        mMailText = (EditText)findViewById(R.id.editText_mail_Activity);
    }

    /**
     * This method set the emails
     */
    private void setEmails(ArrayList emailList){
        mEmailAddress = new String[emailList.size()];
        for(int i = 0;i < emailList.size();i++){
            mEmailAddress[i] = emailList.get(i).toString();
        }
    }

    public int sendMailByIntent() {
        if(mEmailAddress.length != 0){
            String mCc = "cc";
            String mbody = mMailText.getText().toString();
            Intent intent = new Intent(android.content.Intent.ACTION_SEND);
            intent.setType("plain/text");
            intent.putExtra(android.content.Intent.EXTRA_EMAIL, mEmailAddress);
            intent.putExtra(android.content.Intent.EXTRA_CC, mCc);
            intent.putExtra(android.content.Intent.EXTRA_TEXT, mbody);
            startActivity(Intent.createChooser(intent, "mail test"));
        }else{
            Toast.makeText(MailActivity.this, getString(R.string.no_setting_email), Toast.LENGTH_LONG).show();
        }

        return 1;

    }

    /**
     * loadEmailData load wish data from service
     */
    private class loadEmailData extends AsyncTask<Void, Void, ArrayList> {
        private String mUsername;
        String re;

        public loadEmailData() {

        }

        @Override
        protected ArrayList doInBackground(Void... params) {
            //check the input from users.
            try {
                re = mUserService.getEmailData(mNames);
                return readJson(re);
            } catch (Exception e) {

            }
            return readJson(re);
        }

        private ArrayList readJson(String re) {
            ArrayList<String> emails = new ArrayList<String>();
            JSONArray array = null;
            try {
                array = new JSONArray(re);
                Log.d("EMAILJSON", re);
                for (int i = 0; i < array.length(); i++) {
                    WishData data;
                    JSONObject obj = array.getJSONObject(i);
                    String mail = obj.getString(Constant.EMAIL);
                    emails.add(mail);
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }
            return emails;
        }

        @Override
        protected void onPostExecute(ArrayList aVoid) {
            super.onPostExecute(aVoid);
        }
    }

}
