package assign3.id12104865.com.assign3_v065.Model;

/**
 * Created by Jianyu1 on 15/10/20.
 * Object for one member data.
 */
public class MemberData {
    private String mName;
    private String mBirthday;
    private boolean mIsNewWish;
    private boolean mIsNewMessage;



    public MemberData(String mName, String mBirthday, boolean mIsMeal, boolean mIsNewMessage) {
        this.mName = mName;
        this.mBirthday = mBirthday;
        this.mIsNewWish = mIsMeal;
        this.mIsNewMessage = mIsNewMessage;

    }

    public String getmName() {
        return mName;
    }

    public void setmName(String mName) {
        this.mName = mName;
    }

    public String getmBirthday() {
        return mBirthday;
    }

    public void setmBirthday(String mBirthday) {
        this.mBirthday = mBirthday;
    }

    public boolean ismIsNewWish() {
        return mIsNewWish;
    }

    public void setmIsNewWish(boolean mIsNewWish) {
        this.mIsNewWish = mIsNewWish;
    }

    public boolean ismIsNewMessage() {
        return mIsNewMessage;
    }

    public void setmIsNewMessage(boolean mIsNewMessage) {
        this.mIsNewMessage = mIsNewMessage;
    }
}
