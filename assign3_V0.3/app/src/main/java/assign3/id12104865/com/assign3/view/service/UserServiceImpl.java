package assign3.id12104865.com.assign3.view.service;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONObject;

//import assign3.id12104865.com.assign3.view.view.Demo;

/**
 * Created by Jianyu1 on 15/9/26.
 */
public class UserServiceImpl implements UserService {

    /**
     *  Used IP:    --192.168.1.103 (home)
     *              --192.168.1.104 (home)
     *              --172.19.117.236 (UTS)
     */

    private String url = "http://172.19.117.236:8080/jianyu";

    @Override
    public int userLogin(String username, String password) throws Exception {
        String url = "http://172.19.117.236:8080/jianyu/login.do";
        // set Json
        JSONObject object = new JSONObject();
        object.put("username",username);
        object.put("password",password);
        // set parameter
        NameValuePair parameter = new BasicNameValuePair("logindata",object.toString());
        String re = CustomerHttpClient.post(url, parameter);
        // get JSON
        JSONObject jsonResults = new JSONObject(re);
        int result = Integer.parseInt(jsonResults.getString("result"));
        if (result == 1){
                return result;
        }else{
            String errorMsg = jsonResults.getString("errorMsg");
            throw new ServiceRulesException(errorMsg);
        }
    }

    @Override
    public void register(String user_name, String password, String family_code, String isCreator,String famCreator) throws Exception{
        String url = "http://172.19.117.236:8080/jianyu/register.do";
        // set Json
        JSONObject object = new JSONObject();
        object.put("username",user_name);
        object.put("password",password);
        object.put("familyCode",family_code);
        object.put("isCreator",isCreator);
        object.put("familyCreator", famCreator);
        // set parameter
        NameValuePair parameter = new BasicNameValuePair("data",object.toString());
        String re = CustomerHttpClient.post(url, parameter);
        // get JSON
        JSONObject jsonResults = new JSONObject(re);
        int result = Integer.parseInt(jsonResults.getString("result"));
        if (result == 1){

        }else{
            String errorMsg = jsonResults.getString("errorMsg");
            throw new ServiceRulesException(errorMsg);
        }
    }

    @Override
    public String refreshFamilyList(String username, String password) throws Exception{
        //ArrayList<MemberData> memberDatas = new ArrayList<MemberData>();
        String url = "http://172.19.117.236:8080/jianyu/FamilyRfresh";
        // set Json
        JSONObject object = new JSONObject();
        object.put("username",username);
        object.put("password",password);
        // set parameter
        NameValuePair parameter = new BasicNameValuePair("familydata",object.toString());
        String re = CustomerHttpClient.post(url, parameter);
        return re;

        //return memberDatas;
    }
}
