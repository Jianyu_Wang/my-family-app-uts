package assign3.id12104865.com.assign3_v05.Service;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONObject;

import assign3.id12104865.com.assign3_v05.Helper.CustomerHttpClientHelper;

/**
 * Created by Jianyu1 on 15/10/20.
 * Implements UserService to connect to service
 *   Used IPv4:     --192.168.1.103 (home)
 *                  --192.168.1.104 (home)
 *                  --172.19.117.236 (UTS)
 *                  --172.19.152.27 (UTS)
 */

public class UserServiceImpl implements UserService{

    private String ipv4 = "http://172.19.152.27/jianyu/";

    // This method for login (check the name and password from user)
    @Override
    public int userLogin(String username, String password) throws Exception {
        return 0;
    }

    // This method for register (record the name and password from user)
    @Override
    public void register(String user_name, String password, String family_code, String isCreator, String famCreator) throws Exception {
        String url = ipv4 + "register.do";
        // set Json
        JSONObject object = new JSONObject();
        object.put("username",user_name);
        object.put("password",password);
        object.put("familyCode",family_code);
        object.put("isCreator",isCreator);
        object.put("familyCreator", famCreator);
        // set parameter
        NameValuePair parameter = new BasicNameValuePair("data",object.toString());
        String re = CustomerHttpClientHelper.post(url, parameter);
        // get JSON
        JSONObject jsonResults = new JSONObject(re);
        int result = Integer.parseInt(jsonResults.getString("result"));
        if (result == 1){

        }else{
            String errorMsg = jsonResults.getString("errorMsg");
            //throw new ServiceRulesException(errorMsg);
        }
    }

    // This method can refresh list by the new data from service
    @Override
    public String refreshFamilyList(String user_name, String password) throws Exception {
        return null;
    }
}
