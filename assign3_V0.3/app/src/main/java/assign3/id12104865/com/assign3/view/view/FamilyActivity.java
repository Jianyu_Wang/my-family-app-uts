package assign3.id12104865.com.assign3.view.view;

import android.annotation.TargetApi;
import android.content.Context;
import android.content.Intent;
import android.content.res.Resources;
import android.graphics.Color;
import android.graphics.drawable.Drawable;
import android.media.Image;
import android.os.AsyncTask;
import android.os.Build;
import android.os.SystemClock;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.baoyz.widget.PullRefreshLayout;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.lang.reflect.Member;
import java.security.acl.Group;
import java.util.ArrayList;

import assign3.id12104865.com.assign3.R;
import assign3.id12104865.com.assign3.view.service.Constants;
import assign3.id12104865.com.assign3.view.service.MemberData;
import assign3.id12104865.com.assign3.view.service.ServiceRulesException;
import assign3.id12104865.com.assign3.view.service.UserService;
import assign3.id12104865.com.assign3.view.service.UserServiceImpl;

public class FamilyActivity extends ActionBarActivity {

    private ImageButton mPlus_imagebutton;
    private ImageButton mCross_imagebutton;
    private ImageButton mWish_imagebutton;
    private ImageButton mDate_imagebutton;
    private ImageButton mChat_imagebutton;
    private ListView mListView;
    private ArrayList<MemberData> memberDatas = new ArrayList<MemberData>();
    private MemberAdapter adapter;
    private String name;
    private String password;
    private PullRefreshLayout mPullRefreshLayout;
    private UserService mUserService = new UserServiceImpl();

    private void init(){
        mPlus_imagebutton = (ImageButton)findViewById(R.id.imageButton_plus);
        mPlus_imagebutton.setOnClickListener(new buttonClick());
        mCross_imagebutton = (ImageButton)findViewById(R.id.imageButton_cross);
        mCross_imagebutton.setOnClickListener(new buttonClick());
        mWish_imagebutton = (ImageButton)findViewById(R.id.imageButton_wish);
        mWish_imagebutton.setOnClickListener(new buttonClick());
        mDate_imagebutton = (ImageButton)findViewById(R.id.imageButton_date);
        mDate_imagebutton.setOnClickListener(new buttonClick());
        mChat_imagebutton = (ImageButton)findViewById(R.id.imageButton_chat);
        mChat_imagebutton.setOnClickListener(new buttonClick());
        mListView = (ListView)findViewById(R.id.family_listview);
        mPullRefreshLayout = (PullRefreshLayout)findViewById(R.id.swipeRefreshLayout);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_family);
        init();
        getData();
        fillData();
        adapter = new MemberAdapter(this,memberDatas);
        mListView.setAdapter(adapter);
        mPullRefreshLayout.setOnRefreshListener(new PullRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                mPullRefreshLayout.postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        memberDatas.clear();
                        new refreshTask(name,password).execute();
                        mPullRefreshLayout.setRefreshing(false);
                    }
                },3000);
            }
        });
    }

    private void getData(){
        Intent intent = getIntent();
        Bundle bundle = intent.getExtras();
        name = bundle.getString(Constants.USER_NAME);
        password = bundle.getString(Constants.PASSWORD);
    }

    private void fillData(){
        new refreshTask(name,password).execute();
//        MemberData dataone = new MemberData("Jianyu","06/05/1993",true);
//        memberDatas.add(dataone);
//        MemberData datatwo = new MemberData("weiwei","09/25/1993",false);
//        memberDatas.add(datatwo);
//        //Log.d("LISTTAG","this:"+memberDatas.get(0).getName());
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_family, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    private class buttonClick implements View.OnClickListener{
        @Override
        public void onClick(View v) {
            switch (v.getId()){
                case R.id.imageButton_plus:
                    Log.d("TAGG","1");
                    if (mPlus_imagebutton.getVisibility() == View.VISIBLE){
                        mCross_imagebutton.setVisibility(View.VISIBLE);
                        mChat_imagebutton.setVisibility(View.VISIBLE);
                        mWish_imagebutton.setVisibility(View.VISIBLE);
                        mDate_imagebutton.setVisibility(View.VISIBLE);
                        mPlus_imagebutton.setVisibility(View.INVISIBLE);
                        Log.d("TAGG", "2");
                    }
                    break;
                case R.id.imageButton_cross:
                    if (mCross_imagebutton.getVisibility() == View.VISIBLE){
                        mCross_imagebutton.setVisibility(View.GONE);
                        mChat_imagebutton.setVisibility(View.GONE);
                        mWish_imagebutton.setVisibility(View.GONE);
                        mDate_imagebutton.setVisibility(View.GONE);
                        mPlus_imagebutton.setVisibility(View.VISIBLE);
                    }
                    break;
                case R.id.imageButton_wish:
                    Log.d("TAGG", "2");
                    break;
            }
        }
    }

    private class MemberAdapter extends BaseAdapter {

        private LayoutInflater mInflater;
        private Context mContext = null;
        private ArrayList<MemberData> mMemberDataArrayList;


        public MemberAdapter(Context context, ArrayList memberlist){
            this.mContext = context;
            this.mMemberDataArrayList = memberlist;
            mInflater = LayoutInflater.from(context);
        }

        @Override
        public int getCount() {
            return mMemberDataArrayList.size();
        }

        @Override
        public Object getItem(int position) {
            return mMemberDataArrayList.get(position);
        }

        @Override
        public long getItemId(int position) {
            return position;
        }

        @TargetApi(Build.VERSION_CODES.JELLY_BEAN)
        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            View view;
            ViewHolder holder;

            if (convertView == null) {
                view = mInflater.inflate(R.layout.list_item,parent,false);
                holder = new ViewHolder();

                holder.name = (TextView)view.findViewById(R.id.textView_name);
                holder.birthday = (TextView)view.findViewById(R.id.textView_birsthday);
                holder.chatbutton = (ImageButton)view.findViewById(R.id.imageButton_chat_listitem);
                holder.editbutton = (ImageButton)view.findViewById(R.id.imageButton_edit);
                view.setTag(holder);
            }else{
                view = convertView;
                holder = (ViewHolder)view.getTag();
            }


            MemberData memberData = mMemberDataArrayList.get(position);

            holder.name.setText(memberData.getName());
            holder.birthday.setText(memberData.getBirthday());
            if (memberData.getLeaveMessage()){
                Resources resources = mContext.getResources();
                Drawable btnDrawable = resources.getDrawable(R.drawable.conversation);
                holder.chatbutton.setBackground(btnDrawable);
            }
//                        holder.arrival_time.setOnClickListener(new clickEvents(trainData, holder));
            return view;
        }


//        private class clickEvents implements View.OnClickListener{
//
//            ViewHolder mHolder;
//            TrainData mTrainData;
//
//            public clickEvents(TrainData trainData,ViewHolder holder){
//                mHolder = holder;
//                mTrainData = trainData;
//            }
//
//            @Override
//            public void onClick(View v) {
//                int randomNumber = ((int) (Math.random() * 10 + 1));
//                mTrainData.setArrivalTime(mTrainData.getArrivalTime() - randomNumber);
//                new updateData(mHolder).execute();
//
//            }
//        }


        private class ViewHolder{
            public TextView name;
            public TextView birthday;
            public ImageButton chatbutton;
            public ImageButton editbutton;
        }
    }

    private class refreshTask extends AsyncTask<Void,Void,Void> {
        private String mUsername;
        private String mPassword;
        private String result;
        //private String str = getString(R.string.success_login);
        //private boolean success = true;

        public refreshTask(String username,String password){
            mUsername = username;
            mPassword = password;
        }

        @Override
        protected Void doInBackground(Void... params) {
            try {
                result = mUserService.refreshFamilyList(mUsername, mPassword);
                readJson(result);
                publishProgress();
            }  catch (Exception e) {

            }
            return null;
        }

        private void readJson(String re){
            JSONArray array = null;
            try {
                array = new JSONArray(re);
                for (int i = 0; i < array.length(); i++){
                    MemberData data;
                    JSONObject obj = array.getJSONObject(i);
                    String name = obj.getString("name");
                    String birthday = obj.getString("birthday");
                    String message = obj.getString("message");
                    if (message.length() != 0){
                        data = new MemberData(name,birthday,true);
                        data.setMessage(message);
                    }else{
                        data = new MemberData(name,birthday,false);
                    }
                    memberDatas.add(data);
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }

        }

        @Override
        protected void onProgressUpdate(Void... values) {
            super.onProgressUpdate(values);
            adapter.notifyDataSetChanged();
        }

        @Override
        protected void onPostExecute(Void aVoid) {

            super.onPostExecute(aVoid);
        }
    }

}
