package assign3.id12104865.com.assign3_v065.View;

import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.EditText;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.concurrent.ExecutionException;

import assign3.id12104865.com.assign3_v065.Helper.MailHelper;
import assign3.id12104865.com.assign3_v065.Model.Constant;
import assign3.id12104865.com.assign3_v065.Model.WishData;
import assign3.id12104865.com.assign3_v065.R;
import assign3.id12104865.com.assign3_v065.Service.UserService;
import assign3.id12104865.com.assign3_v065.Service.UserServiceImpl;


/**
 *  MailActivity allowed user edit the email details and start system
 *  email app with the email address get from last activity
 */
public class MailActivity extends AppCompatActivity {

    private ArrayList<String> mNames;
    private String[] mEmailAddress;
    private String mEmailBody;
    private EditText mMailText;
    private UserService mUserService = new UserServiceImpl();
    private MailHelper mailHelper;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_mail);

        // get data from last activity
        getData();
        // init the widgets
        init();

        // This method created by AS and called when mail button pressed
        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mailHelper = new MailHelper(mEmailAddress,MailActivity.this,mMailText.getText().toString());
                mailHelper.sendMailByIntent();
            }
        });
    }

    /**
     * This method recive the data from LoginActivity.
     */
    private void getData(){
        Intent intent = getIntent();
        Bundle bundle = intent.getExtras();
        mNames = bundle.getStringArrayList(Constant.MEMBER_NAMES);
        try {
            ArrayList<String> mEmails = new loadEmailData().execute().get();
            setEmails(mEmails);
        } catch (InterruptedException e) {
            e.printStackTrace();
        } catch (ExecutionException e) {
            e.printStackTrace();
        }
    }

    /**
     * This method init widget in this activity.
     */
    private void init(){
        mMailText = (EditText)findViewById(R.id.editText_mail_Activity);
    }

    /**
     * This method set the emails
     */
    private void setEmails(ArrayList emailList){
        mEmailAddress = new String[emailList.size()];
        for(int i = 0;i < emailList.size();i++){
            mEmailAddress[i] = emailList.get(i).toString();
        }
    }

    /**
     * loadEmailData load wish data from service
     */
    private class loadEmailData extends AsyncTask<Void, Void, ArrayList> {
        private String mUsername;
        String re;

        public loadEmailData() {

        }

        @Override
        protected ArrayList doInBackground(Void... params) {
            //check the input from users.
            try {
                re = mUserService.getEmailData(mNames);
                return readJson(re);
            } catch (Exception e) {

            }
            return readJson(re);
        }

        private ArrayList readJson(String re) {
            ArrayList<String> emails = new ArrayList<String>();
            JSONArray array = null;
            try {
                array = new JSONArray(re);
                for (int i = 0; i < array.length(); i++) {
                    WishData data;
                    JSONObject obj = array.getJSONObject(i);
                    String mail = obj.getString(Constant.EMAIL);
                    emails.add(mail);
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }
            return emails;
        }
    }

}
