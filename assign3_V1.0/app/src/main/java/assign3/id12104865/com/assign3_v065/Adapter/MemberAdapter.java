package assign3.id12104865.com.assign3_v065.Adapter;

import android.annotation.TargetApi;
import android.content.Context;
import android.os.Build;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.TextView;

import java.util.ArrayList;

import assign3.id12104865.com.assign3_v065.Model.MemberData;
import assign3.id12104865.com.assign3_v065.R;

/**
 * Created by Jianyu1 on 15/10/20.
 * MemberAdapter support for list view in FamilyActivity
 * that list the family members' information
 */
public class MemberAdapter extends BaseAdapter {

    private LayoutInflater mInflater;
    private Context mContext = null;
    private ArrayList<MemberData> mMemberDataArrayList;


    // constructor
    public MemberAdapter(Context context, ArrayList memberlist){
        this.mContext = context;
        this.mMemberDataArrayList = memberlist;
        mInflater = LayoutInflater.from(context);
    }

    @Override
    public int getCount() {
        return mMemberDataArrayList.size();
    }

    @Override
    public Object getItem(int position) {
        return mMemberDataArrayList.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @TargetApi(Build.VERSION_CODES.JELLY_BEAN)
    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View view;
        final ViewHolder holder;

        // if convertView is null (out of screen) init new one
        if (convertView == null) {
            view = mInflater.inflate(R.layout.list_item,parent,false);
            holder = new ViewHolder();

            holder.name = (TextView)view.findViewById(R.id.textView_name);
            holder.birthday = (TextView)view.findViewById(R.id.textView_birsthday);
            holder.emailBox = (CheckBox)view.findViewById(R.id.checkbox_newmessage_listItem);
            view.setTag(holder);
        }else{
            view = convertView;
            holder = (ViewHolder)view.getTag();
        }

        // update data to list view
        final MemberData memberData = mMemberDataArrayList.get(position);
        holder.name.setText(memberData.getmName());
        holder.birthday.setText(memberData.getmBirthday());
        if (memberData.ismIsNewMessage()){
            holder.emailBox.setVisibility(View.VISIBLE);
        }else{
            holder.emailBox.setVisibility(View.INVISIBLE);
        }
        holder.emailBox.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if(holder.emailBox.isChecked()){
                    memberData.setmIsEmailChecked(true);
                }else{
                    memberData.setmIsEmailChecked(false);
                }
            }
        });
        return view;
    }

    /**
     * ViewHolder help control the view widgets
     */
    private class ViewHolder{
        public TextView name;
        public TextView birthday;
        public CheckBox emailBox;
    }
}
