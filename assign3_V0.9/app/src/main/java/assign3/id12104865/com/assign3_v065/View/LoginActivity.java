package assign3.id12104865.com.assign3_v065.View;

import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.Toast;

import assign3.id12104865.com.assign3_v065.Model.Constant;
import assign3.id12104865.com.assign3_v065.R;
import assign3.id12104865.com.assign3_v065.Service.ServiceRulesException;
import assign3.id12104865.com.assign3_v065.Service.UserService;
import assign3.id12104865.com.assign3_v065.Service.UserServiceImpl;

public class LoginActivity extends AppCompatActivity {

    private ImageButton mRegisterImagebutton;
    private ImageButton mLoginImagebutton;
    private EditText mUserName;
    private EditText mPassword;
    private UserService mUserService = new UserServiceImpl();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        // init widget
        init();
        // set Listeners
        setListeners();
    }

    /**
     * This method init widget in this activity.
     */
    private void init() {
        mRegisterImagebutton = (ImageButton) findViewById(R.id.imagebutton_register_LoginActivity);
        mLoginImagebutton = (ImageButton) findViewById(R.id.imageButton_login_LoginActivity);
        mUserName = (EditText) findViewById(R.id.editText_login_name_LoginActivity);
        mPassword = (EditText) findViewById(R.id.editText_login_password_LoginActivity);
    }

    /**
     * This method set Listeners.
     */
    private void setListeners() {
        // register button clicked
        mRegisterImagebutton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent();
                intent.setClass(LoginActivity.this, RegisterActivity.class);
                startActivity(intent);
//                Intent intent = new Intent();
//                intent.setClass(LoginActivity.this, FamilyActivity.class);
//                intent.putExtra(Constant.NAME, "jason2");
//                intent.putExtra(Constant.PASSWORD, "12345");
//                startActivityForResult(intent, 1);
            }
        });

        // login button clicked
        mLoginImagebutton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String user_name = mUserName.getText().toString();
                String user_password = mPassword.getText().toString();
                new loginTask(user_name, user_password).execute();
            }
        });
    }

    /**
     * LoginTask post infomation to service and check the input from users.
     */
    private class loginTask extends AsyncTask<Void, Void, Void> {
        private String mUsername;
        private String mPassword;
        private String str = getString(R.string.success_login);
        private boolean success = true;

        public loginTask(String username, String password) {
            mUsername = username;
            mPassword = password;
        }

        @Override
        protected Void doInBackground(Void... params) {
            //check the input from users.
            try {
                mUserService.userLogin(mUsername, mPassword);
            } catch (ServiceRulesException e) {
                success = false;
                str = e.toString();
            } catch (Exception e) {
                success = false;
                str = e.toString();
            }
            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            Toast.makeText(LoginActivity.this, str, Toast.LENGTH_LONG).show();
            if (success) {
                Intent intent = new Intent();
                intent.setClass(LoginActivity.this, FamilyActivity.class);
                intent.putExtra(Constant.NAME, mUsername);
                intent.putExtra(Constant.PASSWORD, mPassword);
                startActivityForResult(intent, 1);
            }
            super.onPostExecute(aVoid);
        }
    }


}


//                Intent intent = new Intent();
//                intent.setClass(LoginActivity.this, chatActivity.class);
//                intent.putExtra(Constants.USER_NAME,mUser_name.getText().toString());
//                intent.putExtra(Constants.PASSWORD,mPassword.getText().toString());
//                startActivityForResult(intent,1);
