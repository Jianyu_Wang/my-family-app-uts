package assign3.id12104865.com.assign3.view.service;

public class ServiceRulesException extends Exception {

    public ServiceRulesException(String msg){
        super(msg);
    }
}

